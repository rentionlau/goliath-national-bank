package com.airtouchmedia.gnb.ui.skus.chooser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.airtouchmedia.gnb.R
import kotlinx.android.synthetic.main.sku_item.view.*

class SkuAdapter(
    private val mItems: List<String>,
    private val callBack: SkuCallback
) :
    RecyclerView.Adapter<SkuAdapter.ViewHolder>() {

    private var recyclerView: RecyclerView? = null
    private val onClickListener: View.OnClickListener

    init {
        this.onClickListener = View.OnClickListener {
            callBack.onSkuClicked(
                mItems[recyclerView!!.getChildAdapterPosition(it)],
                it
            )
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.sku_item, parent, false)
        return ViewHolder(view, onClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    inner class ViewHolder constructor(itemView: View, onClickListener: View.OnClickListener) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener(onClickListener)
        }

        fun onBind(position: Int) {
            itemView.textView.text = mItems[position]
        }
    }
}

interface SkuCallback {
    fun onSkuClicked(sku: String, view: View)
}