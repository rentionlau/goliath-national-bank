package com.airtouchmedia.gnb.ui.skus.detailed

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewTreeObserver
import android.view.animation.AccelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.airtouchmedia.gnb.R
import com.airtouchmedia.gnb.Utils
import com.airtouchmedia.gnb.transactions.TransactionsManager
import com.airtouchmedia.gnb.ui.skus.SpaceItemDecoration
import kotlinx.android.synthetic.main.activity_transactions.*
import java.math.RoundingMode


class SkuDetailedActivity : AppCompatActivity() {

    private val DEFAULT_CURRENCY = "EUR"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transactions)

        if (savedInstanceState == null &&
            intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
            intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)
        ) {
            init()
            startRevealAnimation()
        }
    }

    private fun startRevealAnimation() {
        val revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0)
        val revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0)

        val viewTreeObserver = rootLayout.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    rootLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    val finalRadius =
                        (Math.max(rootLayout.width, rootLayout.height) * 1.1).toFloat()

                    // create the animator for this view (the start radius is zero)
                    val circularReveal =
                        ViewAnimationUtils.createCircularReveal(
                            rootLayout,
                            revealX,
                            revealY,
                            0f,
                            finalRadius
                        )
                    circularReveal.duration = 600
                    circularReveal.interpolator = AccelerateInterpolator()

                    // make the view visible and start the animation
                    rootLayout.visibility = View.VISIBLE
                    circularReveal.start()
                }
            })
        }
    }

    fun unRevealActivity() {
        val revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0)
        val revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0)

        val finalRadius = (Math.max(rootLayout.width, rootLayout.height) * 1.1).toFloat()
        val circularReveal = ViewAnimationUtils.createCircularReveal(
            rootLayout, revealX, revealY, finalRadius, 0f
        )

        circularReveal.duration = 400
        circularReveal.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                rootLayout.visibility = View.INVISIBLE
                overridePendingTransition(0, 0)
                finish()
                overridePendingTransition(0, 0)
            }
        })

        circularReveal.start()
    }

    override fun onBackPressed() {
        unRevealActivity()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(
            SpaceItemDecoration(
                Utils.dpToPixels(
                    this,
                    8
                )
            )
        )
        val sku = intent.getStringExtra(SKU)
        recyclerView.adapter = SkuDetailedAdapter(
            TransactionsManager.instance.getAllTransactionForSku(sku)!!,
            layoutInflater
        )

        collapsingToolbar.title = sku
        val totalBigDecimal =
            TransactionsManager.instance.getTotalSumForAllTransactionsForSku(
                sku,
                DEFAULT_CURRENCY
            ).setScale(2, RoundingMode.HALF_EVEN)
        totalPrice_textView.text = "$totalBigDecimal $DEFAULT_CURRENCY"
    }

    companion object {
        private const val SKU = "sku"
        private const val EXTRA_CIRCULAR_REVEAL_X = "reveal_x"
        private const val EXTRA_CIRCULAR_REVEAL_Y = "reveal_y"

        fun showTransactionActivity(
            context: Activity,
            sku: String,
            revealX: Int,
            revealY: Int,
            options: Bundle
        ) {
            val intent = Intent(context, SkuDetailedActivity::class.java)
            intent.putExtra(SKU, sku)
            intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX)
            intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY)
            context.overridePendingTransition(0, 0)
            ActivityCompat.startActivity(context, intent, options)
            context.overridePendingTransition(0, 0)
        }
    }
}