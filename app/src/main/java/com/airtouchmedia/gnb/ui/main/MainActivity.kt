package com.airtouchmedia.gnb.ui.main

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewPropertyAnimator
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import com.airtouchmedia.gnb.R
import com.airtouchmedia.gnb.Utils
import com.airtouchmedia.gnb.currency.CurrencyManager
import com.airtouchmedia.gnb.transactions.TransactionsManager
import com.airtouchmedia.gnb.ui.skus.chooser.SkuChooserActivity
import com.airtouchmedia.gnb.ui.skus.detailed.SkuDetailedActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        startFadeInOutAnimation(waiting_textView,
            Runnable {
                waiting_textView.text = getString(R.string.its_gonna_be)
            },
            Runnable {
                loadCurrencies()
            })
    }

    private fun loadCurrencies() {
        compositeDisposable.add(
            CurrencyManager.instance.loadRatesFromApi()
                .subscribe({
                    onCurrenciesLoaded()
                }, {
                    it.message
                    showErrorSnackbar()
                })
        )
    }

    private fun loadTransactionsFromAPI() {
        compositeDisposable.add(
            TransactionsManager.instance.loadTransactionsFromAPI()
                .subscribe({
                    startFadeInOutAnimation(waiting_textView,
                        Runnable {
                            waiting_textView.text = getString(R.string.dary)
                        },
                        Runnable {
                            startFadeInOutAnimation(
                                waiting_textView,
                                Runnable {
                                    waiting_textView.text =
                                        getString(R.string.its_gonna_be_legendary)
                                },
                                Runnable {
                                    onItsGonnaBeLegendary()
                                }, 0
                            )
                        })
                }, {
                    showErrorSnackbar()
                })
        )
    }

    private fun onItsGonnaBeLegendary() {
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            ObjectAnimator.ofFloat(
                waiting_textView,
                "scaleX",
                1f,
                0.8f,
                0.8f,
                1.5f,
                1.5f,
                1.5f,
                1.5f,
                1.5f,
                1.5f,
                1f
            ),
            ObjectAnimator.ofFloat(
                waiting_textView,
                "scaleY",
                1f,
                0.8f,
                0.8f,
                1.5f,
                1.5f,
                1.5f,
                1.5f,
                1.5f,
                1.5f,
                1f
            ),
            ObjectAnimator.ofFloat(
                waiting_textView,
                "rotation",
                0f,
                -3f,
                -3f,
                3f,
                -3f,
                3f,
                -3f,
                3f,
                -3f,
                0f
            )
        )
        animatorSet.duration = 1100
        animatorSet.interpolator = AccelerateDecelerateInterpolator()
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                startActivity(Intent(this@MainActivity, SkuChooserActivity::class.java))
                finish()
            }
        })
        animatorSet.start()
    }

    private fun onCurrenciesLoaded() {
        startFadeInOutAnimation(
            waiting_textView,
            Runnable {
                waiting_textView.text = getString(R.string.wait_for_it)
            }, Runnable {
                loadTransactionsFromAPI()
            }, delay = 1000
        )
    }

    private fun showErrorSnackbar() {
        val title =
            getString(if (Utils.hasInternetConnexion(this)) R.string.something_went_wrong else (R.string.no_internet))

        val snackbar =
            Snackbar.make(
                rootLayout,
                title,
                Snackbar.LENGTH_INDEFINITE
            )
        snackbar.setAction(
            getString(R.string.retry)
        ) {
            init()
        }
        snackbar.show()
    }

    fun startFadeInOutAnimation(
        view: View,
        afterFadeOutAction: Runnable?,
        afterFadeInAction: Runnable?,
        delay: Long = 0
    ): ViewPropertyAnimator {
        return view.animate()
            .setStartDelay(delay)
            .alpha(0f)
            .setDuration(600)
            .setInterpolator(DecelerateInterpolator())
            .withEndAction {
                afterFadeOutAction?.run()
                view.animate()
                    .setStartDelay(0)
                    .alpha(1f)
                    .setDuration(1000)
                    .setInterpolator(LinearInterpolator())
                    .withEndAction {
                        afterFadeInAction?.run()
                    }
            }
    }
}