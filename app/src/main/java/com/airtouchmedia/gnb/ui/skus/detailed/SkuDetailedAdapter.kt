package com.airtouchmedia.gnb.ui.skus.detailed

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.airtouchmedia.gnb.R
import com.airtouchmedia.gnb.network.enitties.TransactionEntity
import kotlinx.android.synthetic.main.transaction_item_layout.view.*

class SkuDetailedAdapter(
    private var transactionsList: List<TransactionEntity>,
    private val layoutInflater: LayoutInflater
) :
    RecyclerView.Adapter<ViewHolder>() {

    private var recyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            layoutInflater.inflate(R.layout.transaction_item_layout, parent, false)
        )
    }

    override fun getItemCount() = transactionsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(transactionsList[position], position)
    }
}

class ViewHolder(private val view: View) :
    RecyclerView.ViewHolder(view) {

    fun bind(transaction: TransactionEntity, position: Int) {
        view.title_textView.text = "${(position + 1)}. ${transaction.sku}"
        view.price_textView.text = "${transaction.amount} ${transaction.currency}"
    }
}