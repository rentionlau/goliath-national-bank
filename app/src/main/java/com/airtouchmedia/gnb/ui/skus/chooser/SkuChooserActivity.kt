package com.airtouchmedia.gnb.ui.skus.chooser

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import com.airtouchmedia.gnb.R
import com.airtouchmedia.gnb.transactions.TransactionsManager
import com.airtouchmedia.gnb.ui.skus.detailed.SkuDetailedActivity
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import kotlinx.android.synthetic.main.sku_chooser.*

class SkuChooserActivity : AppCompatActivity(), SkuCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.sku_chooser)

        init()
    }

    private fun init() {
        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        recyclerView.layoutManager = layoutManager

        val adapter = SkuAdapter(
            TransactionsManager.instance.getAllDistinctSkus()!!,
            this
        )
        recyclerView.adapter = adapter
    }

    override fun onSkuClicked(sku: String, view: View) {
        val options =
            ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, "transition")
        val array = intArrayOf(0, 0)
        view.getLocationOnScreen(array)
        val revealX = (array[0] + view.width / 2)
        val revealY = (array[1] + view.height / 2)
        SkuDetailedActivity.showTransactionActivity(
            this,
            sku,
            revealX,
            revealY,
            options.toBundle()!!
        )
    }
}