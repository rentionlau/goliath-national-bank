package com.airtouchmedia.gnb.network.enitties

import java.io.Serializable

class TransactionEntity : Serializable {
    var sku: String = ""
    var amount: String = ""
    var currency: String? = ""
}