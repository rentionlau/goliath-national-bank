package com.airtouchmedia.gnb.network.api

import com.airtouchmedia.gnb.network.enitties.RateEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface RatesApi {

    @GET("rates.json")
    fun getAllRates(): Observable<List<RateEntity>>

}