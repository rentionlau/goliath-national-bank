package com.airtouchmedia.gnb.network.api

import com.airtouchmedia.gnb.network.enitties.TransactionEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface TransactionsApi {

    @GET("transactions.json")
    fun getAllTransactions(): Observable<List<TransactionEntity>>

}