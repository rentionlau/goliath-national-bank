package com.airtouchmedia.gnb.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit


object RetrofitManager {

    private const val BASE_URL = "http://gnb.dev.airtouchmedia.com"

    private const val TIMEOUT = 60L

    private val builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

    private fun buildOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
    }

    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = builder.baseUrl(BASE_URL)
            .client(buildOkHttpClient().build())
            .build()

        return retrofit.create(serviceClass)
    }
}