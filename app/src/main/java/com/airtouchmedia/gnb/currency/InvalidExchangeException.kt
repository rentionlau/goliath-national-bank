package com.airtouchmedia.gnb.currency

import java.lang.Exception

class InvalidExchangeException(fromCurrency: String, toCurrency: String) :
    Exception("Invalid exchange rate from $fromCurrency to $toCurrency")