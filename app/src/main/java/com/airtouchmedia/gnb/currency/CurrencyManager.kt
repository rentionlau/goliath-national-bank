package com.airtouchmedia.gnb.currency

import android.util.Log
import com.airtouchmedia.gnb.network.RetrofitManager
import com.airtouchmedia.gnb.network.api.RatesApi
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode

class CurrencyManager {

    /**
     * The map with currencies rates
     * HashMap<FromCurrency, HashMap<ToCurrency, RateRatio>>
     */
    val ratesMap: MutableMap<String, MutableMap<String, BigDecimal>> = mutableMapOf()

    fun loadRatesFromApi(): Completable {
        return Completable.fromCallable {
            val rates =
                RetrofitManager.createService(RatesApi::class.java).getAllRates()
                    .blockingSingle()

            ratesMap.clear()
            rates?.forEach {
                val exchangeMap = if (ratesMap.containsKey(it.from)) {
                    ratesMap[it.from]!!
                } else {
                    mutableMapOf()
                }
                exchangeMap[it.to] = BigDecimal(it.rate)
                ratesMap[it.from] = exchangeMap
            }
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    /**
     *
     * Converts from {@param beforeCurrency} into {@param afterCurrency}.
     *
     * If there is no direct exchange rate between {@param beforeCurrency} and {@param afterCurrency},
     * then
     *
     */
    @Throws(InvalidExchangeException::class)
    fun convert(
        beforeCurrency: String,
        afterCurrency: String,
        value: BigDecimal,
        currenciesAlreadyUsed: ArrayList<String>
    ): BigDecimal {
        if (beforeCurrency == afterCurrency) {
            return value
        }
        // get all available exchanges from the currency we want to exchange
        val allExchangesFromBeforeCurrency =
            ratesMap[beforeCurrency]
                ?: throw InvalidExchangeException(beforeCurrency, afterCurrency)

        val currentExchangeRate = allExchangesFromBeforeCurrency[afterCurrency]
        if (currentExchangeRate == null) {
            allExchangesFromBeforeCurrency.keys.forEach {
                if (ratesMap.containsKey(it) && ratesMap[it]!!.containsKey(afterCurrency)) {
                    return value.multiply(allExchangesFromBeforeCurrency[it])
                        .multiply(ratesMap[it]!![afterCurrency])
                }
            }
            // no exchanges found directly, let's try to exchange to another currencies to get the needed currency
            allExchangesFromBeforeCurrency.keys.forEach {
                try {
                    if (!currenciesAlreadyUsed.contains(it)) {
                        val newValue = value.multiply(allExchangesFromBeforeCurrency[it])
                        currenciesAlreadyUsed.add(it)
                        return convert(it, afterCurrency, newValue, currenciesAlreadyUsed)
                    }
                } catch (exception: Exception) {
                    // try the next one
                }
            }
        } else {
            return value.multiply(currentExchangeRate)
        }

        throw InvalidExchangeException(beforeCurrency, afterCurrency)
    }

    companion object {
        val instance = CurrencyManager()
    }

}