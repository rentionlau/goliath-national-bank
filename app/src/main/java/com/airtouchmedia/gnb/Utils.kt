package com.airtouchmedia.gnb

import android.content.Context
import android.net.ConnectivityManager
import android.util.DisplayMetrics

object Utils {

    fun hasInternetConnexion(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun dpToPixels(context: Context, dp: Int): Int {
        val metrics = context.resources.displayMetrics
        return (dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }
}