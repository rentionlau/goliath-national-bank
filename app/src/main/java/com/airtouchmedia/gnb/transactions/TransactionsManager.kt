package com.airtouchmedia.gnb.transactions

import com.airtouchmedia.gnb.currency.CurrencyManager
import com.airtouchmedia.gnb.network.RetrofitManager
import com.airtouchmedia.gnb.network.api.TransactionsApi
import com.airtouchmedia.gnb.network.enitties.TransactionEntity
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal

class TransactionsManager {

    var transactionsList: List<TransactionEntity>? = null

    fun loadTransactionsFromAPI(): Completable {
        return Completable.fromAction {
            transactionsList =
                RetrofitManager.createService(TransactionsApi::class.java)
                    .getAllTransactions()
                    .blockingSingle()
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getAllDistinctSkus(): List<String>? {
        return transactionsList?.distinctBy {
            it.sku
        }?.map {
            it.sku
        }
    }

    fun getAllTransactionForSku(sku: String): List<TransactionEntity>? {
        return transactionsList?.filter {
            it.sku == sku
        }
    }

    fun getTotalSumForAllTransactionsForSku(sku: String, currency: String): BigDecimal {
        val transactionsForSku = getAllTransactionForSku(sku) ?: return BigDecimal(0)
        var totalSum = BigDecimal(0)
        val currencyManager = CurrencyManager.instance
        transactionsForSku.forEach {
            val currenciesAlreadyUsed = arrayListOf(it.currency!!)

            totalSum = totalSum.add(
                currencyManager.convert(
                    it.currency!!,
                    currency,
                    BigDecimal(it.amount),
                    currenciesAlreadyUsed
                )
            )
        }
        return totalSum
    }

    companion object {
        val instance = TransactionsManager()
    }

}